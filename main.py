# This is a sample Python script.
import json
import os
import sys
from dataclasses import dataclass
from io import BytesIO
from os.path import isfile, join

import PIL
import requests
from PIL import Image, ImageDraw, ImageFont, ImageOps

from const import BESTIARY_DATA_IMAGES, BESTIARY_DATA
from spp.ph import phspprg


@dataclass
class Monster:
    name: str
    size: str
    image: Image


@dataclass
class SimplyMonster:
    name: str
    raw_image: Image


@dataclass
class PaperSize:
    width: int
    height: int


def map_size(size):
    return {
        "T": 150,
        "S": 225,
        "M": 300,
        "L": 600,
        "H": 750,
        "G": 1200,
    }[size]


def create_simply_monster_list() -> list[SimplyMonster]:
    json_name = BESTIARY_DATA_IMAGES[SHORT]
    with open(f"data/{json_name}") as json_file:
        data = json.load(json_file)

    simply_monster_list = []
    for number, d in enumerate(data['monsterFluff']):
        if "images" not in d or d["images"] == None:
            continue

        for i in d["images"]:
            path = i['href']['path']
            img_url = f"https://5e.tools/img/{path}"
            img_data = requests.get(img_url)

            img = Image.open(BytesIO(img_data.content)).convert("RGBA")
            simply_monster_list.append(
                SimplyMonster(
                    raw_image=img,
                    name=d['name']
                )
            )

        sys.stdout.write("\rDownloaded Images:  %d " % number)
        sys.stdout.flush()

    return simply_monster_list.copy()


def get_new_size(x, y, size):
    if x > y:
        d = x / y
        return int(d * map_size(size)), int(map_size(size))
    elif x < y:
        d = y / x
        return int(map_size(size)), int(d * map_size(size))
    else:
        d = y / x
        return int(d * map_size(size)), int(d * map_size(size))


def create_monster_list(simply_monsters: list[SimplyMonster]) -> list[Monster]:
    json_name = BESTIARY_DATA[SHORT]
    with open(f"data/{json_name}") as json_file:
        data = json.load(json_file)

    monsters = data['monster']
    monster_data = []

    for sm in simply_monsters:
        m = next((x for x in monsters if x['name'] == sm.name), None)
        if m is None:
            continue

        if "size" not in m:
            continue

        monster_data.append(Monster(
            name=sm.name,
            size=m['size'][0],
            image=sm.raw_image,
        ))

    return monster_data


def add_margin(pil_img, top, right, bottom, left, color):
    width, height = pil_img.size
    new_width = width + right + left
    new_height = height + top + bottom
    result = Image.new(pil_img.mode, (new_width, new_height), color)
    result.paste(pil_img, (left, top))
    return result


def scale_monster_image(monster: Monster) -> Monster:
    x, y = monster.image.size
    new_x, new_y = get_new_size(x, y, monster.size)
    monster.image = monster.image.resize((new_x, new_y))
    return monster


def create_mirror_image(monster: Monster) -> Monster:
    new_image = Image.new('RGB', (monster.image.size[0], 2 * monster.image.size[1]), (250, 250, 250))
    new_image.paste(monster.image.rotate(180), (0, 0))

    im_mirror_image = ImageOps.mirror(monster.image)
    draw = ImageDraw.Draw(im_mirror_image)

    font = ImageFont.truetype('times', 26)
    _, _, w, h = draw.textbbox((0, 0), monster.name, font=font)
    draw.text(((im_mirror_image.size[0] - w) / 2, (im_mirror_image.size[1] - 40)), monster.name, font=font,
              fill=(0, 0, 0))

    new_image.paste(im_mirror_image, (0, int(new_image.size[1] / 2)))
    # new_image.save(f"rezized/MM_{monster.name}.jpg")

    monster.image = new_image

    return monster


def create() -> list[Monster]:
    simply_monster_monsters = create_simply_monster_list()
    monsters = create_monster_list(simply_monster_monsters)

    for m in monsters:
        m = scale_monster_image(m)
        m.image = add_margin(m.image, 0, 0, 50, 0, (255, 255, 255))
        create_mirror_image(m)

    return monsters


def filter_too_large(monsters: list[Monster], papersize: PaperSize) -> list[Monster]:
    for m in monsters:
        if m.image.size[1] > papersize.height:
            print(m.name)
            monsters.remove(m)

    return monsters


def save_all_single(monsters: list[Monster]):
    for number, m in enumerate(monsters):
        m.image.save(f"rezized/{SHORT}_{m.name}.jpg")


def save_in_groups(monsters: list[Monster], papersize: PaperSize, n=1):
    boxes = [[m.image.size[0], m.image.size[1]] for m in monsters]

    new_height, rectangles = phspprg(papersize.width, boxes, sorting="height")
    rectangles = sorted(rectangles, key=lambda x: (x.y, x.x))

    xxx = Image.new('RGB', (papersize.width, papersize.height), (250, 250, 250))

    for rt in rectangles:
        m = next((x for x in monsters if x.image.size[0] == rt.w and x.image.size[1] == rt.h), None)
        if m is None:
            m = next((x for x in monsters if x.image.size[1] == rt.w and x.image.size[0] == rt.h), None)
            m.image = m.image.rotate(90, PIL.Image.NEAREST, expand=1)

        if rt.y > papersize.height or rt.y + m.image.size[1] > papersize.height:
            save_in_groups(monsters.copy(), papersize, n + 1)
            break

        monsters.remove(m)
        xxx.paste(m.image, (rt.x, rt.y))

    xxx.save(f"monster_marged/{SHORT}_{n}.png")


def safe_to_pdf():
    mypath = "monster_marged"

    onlyfiles = [f for f in os.listdir(mypath) if isfile(join(mypath, f))]

    if len(onlyfiles) == 0:
        raise "no files to convert"

    image_1 = Image.open(f"{mypath}/{onlyfiles[0]}")
    im_1 = image_1.convert('RGB')
    image_list = []
    onlyfiles.pop(0)
    for d in onlyfiles:
        image = Image.open(f"{mypath}/{d}")
        im = image.convert('RGB')
        image_list.append(im)

    im_1.save(f'{SHORT}.pdf', save_all=True, append_images=image_list)


def clear_folder():
    dir = 'monster_marged'
    if not os.path.exists(dir):
        os.makedirs(dir)

    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))

    dir = 'rezized'
    if not os.path.exists(dir):
        os.makedirs(dir)
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))


def main():
    clear_folder()
    monsters = create()
    save_all_single(monsters)
    paper = PaperSize(2480, 3508)
    monsters = filter_too_large(monsters, paper)
    save_in_groups(monsters, paper)
    safe_to_pdf()


if __name__ == '__main__':
    SHORT = "FTD"
    main()
